"""
Python @ Home

My Little Zoo - working with lists and dicts

In this exercise, we will organize some animals in our imaginary little zoo.

You may open this file in Spyder and it should run just fine - it's a Python
script. Just follow the comments step by step and print() your variable wherever
you see fit. You should be able to execute the whole script after each step.

Hint: You are not supposed to change the code already written. If your job is to
change a variable, use some additional code to do that.

You may find (one possible) solution here:
https://python.tyto.xyz/day1/homework_solution.py
"""

# This is a list of birds:
birds = ["Robin", "Falcon", "Eagle", "Owl"]

# Save the _first_ bird as variable `songbird`:


# Save the other birds (all but the first) as a list `birdsofprey`:


# Add "Pigeon" at the end of `birds`:


# Create a list with mammals, at least three, e.g. Mouse, Elefant, Bear, Tiger:


# Concatenate `birds` and `mammals` to one list called `animals`:


# Print the number of animals (length of `animals`):


# Print the "second to last" animal in the list:


# Print a list of every second animal (hint: use indexing with a step size):


# Save `animals` to another variable called `fauna`


# Uncomment/execute the following line, then print `animals` and `fauna`
#fauna.remove("Eagle")


# Find a way to *copy* `animals` to an independent list `fauna` (and try if it worked):


"""
Now let's move on to use a dictionary
"""

# We want to organize our zoo. It already has a name and some animals (uncomment/execute):
#zoo = {
#    'name': "Python Zoo",
#    'birds': birds,
#    'mammals': mammals,
#}

# Change the name of your zoo:


# Add the opening hours as two separate keys (`opens`, `closes`) - it's okay to use two lines!


# Print a text like "Welcome to Python Zoo! Open: 9am to 5pm", using the values
# of the `zoo` dictionary (hint: use `+` to concatenate strings):


# Make zoo['fish'] a list of fish (e.g. Shark, Salmon, Carp):


# Print your entire `zoo` dictionary:


# Append "Lion" to the `mammals` variable:


# Print `zoo['mammals']` to see if it has changed, too:


# Uncomment/execute the following line, then print `mammals` and `zoo['mammals']`
#mammals.sort()


# Uncomment/execute the following line, then print `birds` and `zoo['birds']`
#zoo['birds'] = sorted(zoo['birds'])


# See this question and the accepted answer on stackoverflow:
# https://stackoverflow.com/questions/22442378/what-is-the-difference-between-sortedlist-vs-list-sort
#
# Try to predict what the following statements would do and print at this point.
# Then uncomment/execute them to test your predictions...

#fish = zoo['fish']
#print(fish)
#fish.append('Clownfish')
#print(fish)
#print(zoo['fish'])

