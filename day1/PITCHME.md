
# @css[darkblue](Python Course)

## @css[lightblue](Day 1)

```python
>>> print("Python is fun!")
```

@snap[south reference]
Slides: https://python.tyto.xyz/day1/
@snapend

---

### Today

* simple math and string operations
* variables and assignments
* common built-in variable types
    - `int`, `float`
    - `tuple`, `list`, `dict`
* indexing

@css[fragment](Featuring today: _Python Console_ and _Spyder_)

@snap[north-east icons]
![](https://upload.wikimedia.org/wikipedia/commons/c/c3/Python-logo-notext.svg)
![](https://upload.wikimedia.org/wikipedia/commons/7/7e/Spyder_logo.svg)
@snapend

---

@snap[north-west half]
__Start Anaconda Prompt:__
<br /><br />
@fa[caret-right] Open Start Menu
<br />
@fa[caret-right] Type "anaconda"
<br />
@fa[caret-right] Click _Anaconda Prompt_
@snapend

@snap[east half]
![](day1/anaconda_prompt_start.png)
@snapend

---

### The Anaconda Prompt

![](day1/anaconda_prompt_initial.png)

---

#### Useful commands

* `cd dirname` @fa[arrow-right] change to directory _dirname_
* `mkdir dirname` @fa[arrow-right] make/create directory _dirname_
* @fa[windows] `dir` @fa[arrow-right] list directory content
* @fa[windows] `c:`, `d:`, ... @fa[arrow-right] change drive
* @fa[windows] `del dir_or_file` @fa[arrow-right] delete directory or file
* @fa[linux]/@fa[apple]: `ls`/`ls -lha` @fa[arrow-right] list directory content
* @fa[linux]/@fa[apple]: `rm file`/`rm -r dir` @fa[arrow-right] remove file/directory
* `exit` @fa[arrow-right] close Anaconda Prompt

---

### Python Command Line:

@snap[midpoint lightblue]
@fa[caret-right] Type "python"
<br />
@fa[caret-right] Hit _Enter_ (or _Return_)
@snapend

![](day1/anaconda_prompt_python.png)

---

@fa[caret-right] Type `print("Hello, World!")`

![](day1/anaconda_prompt_python_helloworld.png)

@snap[south fragment box remember]
Hit _Enter_ to execute a statement
@snapend

---

@fa[caret-right] To exit Python, type `quit()` or `exit()`

![](day1/anaconda_prompt_python_helloworld_quit.png)

---

### Python Command Line

#### - an _over-powered_ calculator -

@snap[fragment]
![](https://media1.tenor.com/images/a1c4395f680cc71fbb361da3f0a17cc9/tenor.gif?itemid=9745969)
@snapend

---

@snap[north box exercise]
Try yourself!
@snapend

__Addition & Subtraction__

```python
>>> 5 + 3
>>> 12 - 4
>>> 12 - 9 + 5
>>> -3 + 11
```

__Multiplication & Exponentiation__

```python
>>> 2 * 4
>>> 2 * 2 * 2
>>> 2 ** 3
>>> -1 * 4 * -2
>>> 2 ** 2 * 2
```

@snap[south box]
Leading @css[hljs-meta](`>>>`) indicate _Command Line_ input. Don't type them.
@snapend

---

_Comments on_

### Addition & Subtraction

```python
>>> 2 + 3
>>> 7 - 3
>>> 5 - 7
>>> 5 + 3 + 7 - 4 + 12 - 10
>>> -10
>>> -5 + 3
>>> 100 + -5
>>> +100 - 5
```
@[1-4](add/subtract two (or _more_) integers)
@[5-7](use a `-` to indicate _negative_ numbers)
@[8](you may use a `+` to emphasize a _positive_ number)

---

_Comments on_

### Multiplication & Exponentiation

```python
>>> 2 * 3
>>> 2 * 3 * 5
>>> 2 * -3
>>> 2 * 3 + 5
>>> 2 + 3 * 5
>>> 2 ** 3 + 2
>>> (2 + 3) * 5
>>> (2 + 3) * (5 - 5)
>>> 2 + (3 * 5) - 5
>>> (9 - 5) ** 4
```
@[1-2](multiply two or more integers)
@[3](negative integers in multiplication are fine)
@[4-6](Python follows common _order of operations_:<br />`**` then `*` and `/` then `+` and `-`)
@[7-10](Parenthesis take the highest _precedence_)

@css[fragment box question](And what about @color[darkorange](division)?)

---

### Division

```python
>>> 6 / 2
>>> 11 / 5
>>> 2 / -3
>>> 2 / 3 + 5
>>> 2 + 3 / 5
>>> (2 + 3) / 5
>>> 6 * 2 / 3
>>> 2 * 3 + 12 / 4 - 5
```
@[1-8](basically, everything works as expected)
@[:](Try it yourself!)
@[1](Why is the result of `6 / 2` then `3.0` and not `3`?)

---

### The problem with divisions

A division of two **integers** _could_ result  
in a **non-integer** faction, like  
`5 / 2` @fa[arrow-right] `2.5`

Therefore, divisions of integers _always_ result  
in a **decimal number**:

`5 / 2` @fa[arrow-right] `2.5`  
`6 / 2` @fa[arrow-right] `3.0`

@css[fragment box remember](Division of integers _always_ results a `float`)

---

### Division resolved

`truediv` or `/` __vs.__ `floordiv` or `//`

@css[south box remember fragment](`#` indicate a _comment_. Text after the `#` is not executed)

```python
>>> # truediv:
>>> 6 / 2
>>> 11 / 5
>>> # floordiv:
>>> 6 // 2
>>> 11 // 5
```
@[1-3](`/` works as we have seen before: correct result)
@[4-6](`//` returns a _floored_ result of the _same type_)
@[:]

@css[box fragment](This behavior was different in Python 2)


---

## Data types

![](https://media1.tenor.com/images/57f519a654b780fefee381de9c9b4b2d/tenor.gif?itemid=8569288)

---

### (built-in) Numeric types

* Integers
* Floats
* Complex numbers

---

1) __Integer__ or `int`

_"whole" numbers_, &#x2124;

Used for basic calculations and _indexing_

```python
-2, -1, 0, 1, 2, 3
-12345, 65536, 18446744073709551615
2 + 3, 6 - 1, -1 * 5, 10 ** 6
int('3'), int(42.0)
```
@[1-3](negative and positive)
@[1](include zero (always "positive"))
@[2](may be pretty big, actually there is __no limit__)
@[1-2](can be written as literals)
@[3](can also be used in calculation, return an `int`)
@[4](may be derived from other types, i.e. _casted_)
@[:]

@css[south box danger fragment](Do not confound the `,` "comma" and the `.` "decimal point")

---

2) __Floating point number__ or `float`

_decimal numbers_ / _real numbers_, &#x211a;

Used for most _scientific_ calculations

```python
-1.0, -0.0, 0.0, 0.0000001, .5, 1.5, 2.345, 3.14159265
-123.45, 65536, 1.2345e100, 2**(-149), 2**1023, 8.98E+307
-math.inf, +math.inf, math.nan
2. + 3., 6 - 1.0, -1.2 * 5, 2.0 ** 6, 10 / 2, 2 / 3, 10.0 // 5
float(123), float('3'), float('3.123E+123'), float('3.141')
```
@[1-3](negative and positive)
@[1](include zero (and "negative zero"))
@[2](may be pretty big and small)
@[2](precise to approx. 15~16 _decimal_ digits)
@[3](include negative and positive _infinity_)
@[1-2](can be written as literals)
@[4](can also be used in calculation, return a `float`)
@[5](may be derived from other types, i.e. _casted_)
@[:]

---

### Strings or `str`

Used to contain (nearly all sorts of) text.

Surrounded by single `'` or double `"` quotation marks:

```python
"foo", "bar"
"I am a string"
'I am a string, too'
"I'm also a string"
"", '', "   ", '   '
'0.00', "To my left is also a string"
str(12345), str(1.2345e4)
```

---

#### Common `str` operations

@css[exercise fragment](Try it yourself!)

```python
"Hello," + ' World!'
'a' * 10, 10 * 'b'
"     Peter   ".strip()
"-----Peter---".strip('-')
len("Peter")
"THis iS oNlY hARdLy REadABlE".lower()
"THis iS oNlY hARdLy REadABlE".upper()
"THis iS oNlY hARdLy REadABlE".title()
"Hello, World!".replace("World", "Peter")
"Peter,John,Nancy,Sophie".split(",")
```

---

## Variables

Now that we have _values_ of different _types_ we want to 

* remember them
* use them

That is, what _variables_ are designed for!

---

### Variables - Assignments

@css[box remember](A _value_ is assigned to a _variable_)

```python
foo = 3.141
bar = 5 + 3 / 2
name = "Roland"
age = 33
size = 1.70 # meters
phone = "+1" + str(347) + "23" + str(6 * 6)
```

---

### Variables - Usage

Once assigned, variables can be used just like _literals_:

```python
a = 2
b = 3
d = a * b + 23
s = str(d)
t = "The result of 'a * b + 23' is " + s
print(a, b)
print(t)
```

---

## Output Variables

You want to know the contents of a variable or the result of an operation?

__`print` it!__

```python
foo = 5
print() # print an empty line
print("Hello, World!")
print(foo)
print("foo and it's square:", foo, foo**2)
```

@css[box remember fragment](You can `print` literally everything!<br />It will be converted to a `str` for you.)

---

@css[exercise](Time to get to work!)

__1) Create three variables:__

* Integer: `age`
* String: `name`
* Float: `size` in meters

__2) Print__ (using your variables)

* A nice greating, like "Hello, Peter!"
* Your age and size
* The sum of your age and the length of you name
* The above, multiplied with your size

@css[box](Pay attention to the right datatype.)

---

## Variables containing multiple values

__Sequences of values:__

Tuples or Lists (`tuple`, `list`)

__Key/Value pairs:__

Dictionaries (`dict`)

---

### Tuple

```python
foo = "this", "is", "a", "tuple"
bar = (1, 2, 3)
mixed = ("Here are some numbers", 1, 2, 3)
moremixed = (1, 5.0, ("tuple", ("one", "two", "three")))
```

* Defined as comma-separated values
* Usually surrounded by parenthesis `(...)`
* Can hold any variable type
* Once defined, a tuple **cannot be changed**
* Supports indexing with `int`

---

### Indexing tuples

@css[box remember](Indexes in Python are zero-based)

```python
foo = ("one", "two", "three", "four", "five")
# One value by positive index:
foo[0]    # -> "one"
foo[2]    # -> "three"
# Negative indexes (from the end)
foo[-1]   # -> "five"
foo[-4]   # -> "two"
# Slices:
foo[1:4]  # -> ("two", "three", "four")
foo[2:-1] # -> ("three", "four")
foo[:2]   # -> ("one", "two")
foo[3:]   # -> ("four", "five")
foo[:]    # -> the entire tuple
foo[::-1] # -> the entire tuple in reverse order
```

---

### List

```python
foo = ["this", "is", "a", "list"]
bar = [1, 2, 3]
mixed = ["Here are some numbers", 1, 2, 3]
moremixed = [1, 5.0, ["tuple", ("one", "two", "three")]]
```

* comma-separated, square brackets `[...]`
* Can hold any variable type
* Once defined, a list **can be changed**
* Supports indexing with `int`

---

### List operations I

The following statements return _new_ lists:

```python
foo = [1, 2, 3]
bar = ["one", "two", "three"]
# Slicing:
foo[:2] # -> [1, 2]
bar[:] # -> a COPY of bar
# 'add' or concatenate two lists:
foo + bar # -> [1, 2, 3, "one", "two", "three"]
# 'multiply' a list with an integer `n`, concatenates the list `n` times:
foo * 3 # -> [1, 2, 3, 1, 2, 3, 1, 2, 3]
```

---

### List operations II

The following statements _change_ a list _in place_:

```python
foo = [1, 2, 3]
bar = ["one", "two", "three"]
# Change/replace a value:
bar[0] = "uno" # bar -> ["uno", "two", "three"]
# Replace a slice:
bar[1:3] = ['dos', 'tres'] # bar -> ["uno", "dos", "tres"]
# Add a value at the end:
foo.append(6) # foo -> [1, 2, 3, 6]
# Add a value at specified position:
foo.insert(2, "x") # foo -> [1, 2, 'x', 3, 6]
# Add all values of another list:
foo.extend(bar) # foo -> [1, 2, 'x', 3, 6, "uno", "dos", "tres"]
```

---

### Dictionary

```python
foo = {"key1": "value", "key2": "value2"}
bar = {1: 10, 2.0: 100, -3: 1000}
mixed = {"name": "John", "age": 33, 21: "yes"}
moremixed = {"name": "table",
    dimensions: {"h": 29.5, "l": 30.0, "w": 48.0},
    "categories": ["work", "desk", "computer"],
}
```

* key/value pairs in curly braces `{...}`
* Values of any variable type
* Keys can be numbers, strings, and more
* Values can be changed, added or removed
* Supports indexing with the given keys

---

### Dictionary operations

```python
points = {'Peter': 505, 'Mary': 489, 'Alice': 517}
# indexing / access a value by key:
points["Peter"] # -> 505
# change or add a value:
points['Mary'] = 496 # points['Mary'] -> 496
points['Alice'] += 2 # points['Alice'] -> 519, i.e. 517 + 2
points['John'] = 512
# Remove a value
del point['Mary']
# Add multiple values (from another dict)
point.update({'Kevin': 504, 'Jane': 523})
```

---

# End
