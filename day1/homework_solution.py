"""
Python @ Home

My Little Zoo - working with lists and dicts

In this exercise, we will organize some animals in our imaginary little zoo.

You may open this file in Spyder and it should run just fine - it's a Python
script. Just follow the comments step by step and print() your variable wherever
you see fit. You should be able to execute the whole script after each step.

Hint: You are not supposed to change the code already written. If your job is to
change a variable, use some additional code to do that.

You may find (one possible) solution here:
https://python.tyto.xyz/day1/homework_solution.py
"""

# This is a list of birds:
birds = ["Robin", "Falcon", "Eagle", "Owl"]

# Save the _first_ bird as variable `songbird`:
songbird = birds[0]
print("songbird:", songbird)

# Save the other birds (all but the first) as a list `birdsofprey`:
birdsofprey = birds[1:]
print("birdsofprey:", birdsofprey)

# Add "Pigeon" at the end of `birds`:
birds.append("Pigeon")
print("birds:", birds)

# Create a list with mammals, at least three, e.g. Mouse, Elefant, Bear, Tiger:
mammals = ["Mouse", "Elefant", "Bear", "Tiger"]
print("mammals:", mammals)

# Concatenate `birds` and `mammals` to one list called `animals`:
animals = birds + mammals
print("animals:", animals)

# Print the number of animals (length of `animals`):
print("Number of animals:", len(animals))

# Print the "second to last" animal in the list:
print("second to last animal:", animals[-2])

# Print a list of every second animal (hint: use indexing with a step size):
print("every second animal:", animals[::2])

# Save `animals` to another variable called `fauna`
fauna = animals
print("animals:", animals)
print("fauna:", fauna)

# Uncomment/execute the following line, then print `animals` and `fauna`
fauna.remove("Eagle")
print("Eagle removed from `fauna`...")
print("animals:", animals)
print("fauna:", fauna)

# Find a way to *copy* `animals` to an independent list `fauna` (and try if it worked):
fauna = animals[:]
# alternatives:
#fauna = animals.copy()
#fauna = list(animals) # the list() function always generates a new list
#fauna = animals + [] # adding a list, even an empty list, generates a new list
fauna.append('Raccoon')
print("Raccoon added to `fauna`...")
print("animals:", animals)
print("fauna:", fauna)

"""
Now let's move on to use a dictionary
"""

# We want to organize our zoo. It already has a name and some animals:

zoo = {
    'name': "Python Zoo",
    'birds': birds,
    'mammals': mammals,
}

# Change the name of your zoo:
zoo['name'] = "Roland's Zoo"

# Add the opening hours as two separate keys (`opens`, `closes`) - it's okay to use two lines!
zoo['opens'] = "9am"
zoo['closes'] = "5pm"
# alternative in one line:
#zoo.update({'opens': "9am", 'closes': "5pm"})

# Print a text like "Welcome to Python Zoo! Open: 9am to 5pm", using the values
# of the `zoo` dictionary (hint: use `+` to concatenate strings):
print("Welcome to " + zoo['name'] + "! Open: " + zoo['opens'] + " to " + zoo['closes'])

# Make zoo['fish'] a list of fish (e.g. Shark, Salmon, Carp):
zoo['fish'] = ['Shark', 'Salmon', 'Carp']

# Print your entire `zoo` dictionary:
print(zoo)

# Append "Lion" to the `mammals` variable:
print("mammals without lion:", mammals)
mammals.append("Lion")
print("mammals  with   lion:", mammals)

# Print `zoo['mammals']` to see if it has changed, too:
print(zoo['mammals'])

# Uncomment/execute the following line, then print `mammals` and `zoo['mammals']`
mammals.sort()
print("After: mammals.sort()")
print("mammals:", mammals)
print("zoo['mammals']:", zoo['mammals'])


# Uncomment/execute the following line, then print `birds` and `zoo['birds']`
zoo['birds'] = sorted(zoo['birds'])
print("After: zoo['birds'] = sorted(zoo['birds'])")
print("birds:", birds)
print("zoo['birds']:", zoo['birds'])


# See this question and the accepted answer on stackoverflow:
# https://stackoverflow.com/questions/22442378/what-is-the-difference-between-sortedlist-vs-list-sort
#
# Try to predict what the following statements would do and print at this point.
# Then uncomment/execute them to test your predictions...

# The following line saves a new REFERENCE to the SAME list:
fish = zoo['fish']
print(fish)
# The lists `fish` and `zoo['fish']` are IDENTICAL objects, therefore, appending to one of them changes the other as well:
fish.append('Clownfish')
print(fish)
print(zoo['fish'])

